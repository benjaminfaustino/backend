<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Gallery, Input, Batch, File;

use View, Response;
use CreateGallery, UpdateGallery;
use CreateGalleryRequest, UpdateGalleryRequest;

use Controller as Controller;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view::make('admin/gallery/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->dispatch(new CreateGallery( $request->all(), Input::file() ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGalleryRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateGallery( $request->all() , Input::file(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::find($id)->delete();
    }

    public function get()
    {
        // $data = Gallery::orderBy('created_at', 'desc')->get();

        $data = Gallery::all();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function bulk(Request $request){

        Batch::update('galleries', $request->all(), 'id');

    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Registration, Input, Batch, File;

use View, Response;
use CreateRegistration, UpdateRegistration;
use CreateRegistrationRequest, UpdateRegistrationRequest;

use Controller as Controller;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view::make('admin/registration/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRegistrationRequest $request)
    {
        $data = $this->dispatch(new CreateRegistration( $request->all(), Input::file() ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegistrationRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateRegistration( $request->all() , Input::file(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Registration::find($id)->delete();
    }

    public function get()
    {
        // $data = Registration::orderBy('created_at', 'desc')->get();

        $data = Registration::all();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function bulk(Request $request){

        Batch::update('registrations', $request->all(), 'id');

    }

    public function count(Request $request){

        $data = Registration::where('status', 'new')->count();

        return Response::json(
            [   
                'data' => $data,
            ]
        );

    }
}

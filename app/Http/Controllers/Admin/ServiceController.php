<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Service, Input, Batch, File;

use View, Response;
use CreateService, UpdateService;
use CreateServiceRequest, UpdateServiceRequest;

use Controller as Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view::make('admin/service/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateServiceRequest $request)
    {
        $data = $this->dispatch(new CreateService( $request->all(), Input::file() ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateService( $request->all() , Input::file(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Service::find($id)->delete();
    }

    public function get()
    {
        // $data = Service::orderBy('created_at', 'desc')->get();

        $data = Service::all();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function bulk(Request $request){

        Batch::update('services', $request->all(), 'id');

    }
}

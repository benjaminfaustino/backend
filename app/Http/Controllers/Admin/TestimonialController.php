<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Testimonial, Input, Batch, File;

use View, Response;
use CreateTestimonial, UpdateTestimonial;
use CreateTestimonialRequest, UpdateTestimonialRequest;

use Controller as Controller;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view::make('admin/testimonial/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTestimonialRequest $request)
    {
        $data = $this->dispatch(new CreateTestimonial( $request->all(), Input::file() ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTestimonialRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateTestimonial( $request->all() , Input::file(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Testimonial::find($id)->delete();
    }

    public function get()
    {
        // $data = Testimonial::orderBy('created_at', 'desc')->get();

        $data = Testimonial::all();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function bulk(Request $request){

        Batch::update('testimonials', $request->all(), 'id');

    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use User, Input, Batch, File;

use View, Response;
use CreateUser, UpdateUser;
use CreateUserRequest, UpdateUserRequest;

use Controller as Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view::make('admin/user/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $this->dispatch(new CreateUser( $request->all(), Input::file() ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateUser( $request->all() , Input::file(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }

    public function get()
    {
        // $data = User::orderBy('created_at', 'desc')->get();

        $data = User::where('role', '!=', 'superadmin')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function bulk(Request $request){

        Batch::update('users', $request->all(), 'id');

    }
}

<?php 

namespace App\Http\Requests\Banners;

use App\Http\Requests\GlobalRequest as Request;

class CreateBannerRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required',
		];
	}

	// public function messages()
	// {
	// 	return [
	// 		'title.required' => 'Slide Title is required!',
	// 	];
	// }

}

<?php 

namespace App\Http\Requests;

use App\Http\Requests\Request as BaseRequest;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

abstract class GlobalRequest extends BaseRequest {

	private $requireJson;

	/**
	 * Get the response for a forbidden operation.
	 * 
	 * @override \Illuminate\Foundation\Http\FormRequest
	 * @return \Illuminate\Http\Response
	 */
	public function forbiddenResponse()
	{
		// run through Exception handler
 		return abort(403);
	}

	public function forceJsonOnFail()
	{
		$this->requireJson = true;
	}

	public function responseRequiresJson()
	{
		return $this->requireJson;
	}

}

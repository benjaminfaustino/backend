<?php 

namespace App\Http\Requests\Registrations;

use App\Http\Requests\GlobalRequest as Request;

class CreateRegistrationRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'firstname' 		=> 'required',
			'lastname'  		=> 'required',
			'cellphone_number'  => 'required',
			'address'  			=> 'required',
		];
	}

	// public function messages()
	// {
	// 	return [
	// 		'title.required' => 'Slide Title is required!',
	// 	];
	// }

}

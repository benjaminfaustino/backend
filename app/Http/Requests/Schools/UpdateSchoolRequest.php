<?php 

namespace App\Http\Requests\Schools;

use App\Http\Requests\GlobalRequest as Request;

class UpdateSchoolRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 				=> 'required',
			'description'  		=> 'required',
			'email' 			=> 'email|unique:schools,email, '. $this->id .' ',
		];
	}

	// public function messages()
	// {
	// 	return [
	// 		'title.required' 		=> 'Slide Title is required!',
	// 	];
	// }

}

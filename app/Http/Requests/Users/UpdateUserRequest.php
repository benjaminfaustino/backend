<?php 

namespace App\Http\Requests\Users;

use App\Http\Requests\GlobalRequest as Request;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'firstname' => 'required',
			'lastname' => 'required',
			'email' => 'email|unique:users,email, '. $this->id .' ',
			'location'  => 'required',
			'role'  => 'required',
		];
	}

	// public function messages()
	// {
	// 	return [
	// 		'title.required' 		=> 'Slide Title is required!',
	// 	];
	// }

}

<?php 

namespace App\Jobs\Banner;

use Banner;
use App\Jobs\Job;

use Uploader;

class CreateBanner extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/banners/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'title' 		=> $this->content['title'],
			'filename'   	=> $filename,
			'sort' 			=> $this->content['sort'],
			'status' 	    => $this->content['status'],
		];

		return Banner::create($input);

	}

}

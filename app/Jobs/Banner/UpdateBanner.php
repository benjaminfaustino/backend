<?php 

namespace App\Jobs\Banner;

use Banner;
use App\Jobs\Job;

use Uploader;

class UpdateBanner extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = Banner::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/banners/');
		} else {
			$filename = $data->filename;
		}
       
		$data->title 		= $this->content['title'];
		$data->filename   	= $filename;
		$data->sort 		= $this->content['sort'];
		$data->status	    = $this->content['status'];

        $data->save();

        return $data;

	}

}

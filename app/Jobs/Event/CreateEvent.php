<?php 

namespace App\Jobs\Event;

use Event;
use App\Jobs\Job;

use Uploader;

class CreateEvent extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/events/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'title' 		=> $this->content['title'],
			'description'  	=> $this->content['description'],
			'date' 			=> $this->content['date'],
			'filename' 		=> $filename,
			'sort' 			=> $this->content['sort'],
			'status' 		=> $this->content['status'],
			'location' 		=> $this->content['location'],
		];

		return Event::create($input);

	}

}

<?php 

namespace App\Jobs\Event;

use Event;
use App\Jobs\Job;

use Uploader;

class UpdateEvent extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = Event::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/events/');
		} else {
			$filename = $data->filename;
		}
       
		$data->title 		= $this->content['title'];
		$data->description  = $this->content['description'];
		$data->date 		= $this->content['date'];
		$data->filename 	= $filename;
		$data->sort 		= $this->content['sort'];
		$data->status 		= $this->content['status'];
		$data->location 	= $this->content['location'];

        $data->save();

        return $data;

	}

}

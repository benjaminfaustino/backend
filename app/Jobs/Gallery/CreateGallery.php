<?php 

namespace App\Jobs\Gallery;

use Gallery;
use App\Jobs\Job;

use Uploader;

class CreateGallery extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/galleries/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'title' 		=> $this->content['title'],
			'description'  	=> $this->content['description'],
			'filename' 		=> $filename,
			'sort' 			=> $this->content['sort'],
			'status' 		=> $this->content['status'],
			'location' 		=> $this->content['location'],
		];

		return Gallery::create($input);

	}

}

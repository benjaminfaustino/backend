<?php 

namespace App\Jobs\Gallery;

use Gallery;
use App\Jobs\Job;

use Uploader;

class UpdateGallery extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = Gallery::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/galleries/');
		} else {
			$filename = $data->filename;
		}
       
		$data->title 		= $this->content['title'];
		$data->description  = $this->content['description'];
		$data->filename   	= $filename;
		$data->sort			= $this->content['sort'];
		$data->status		= $this->content['status'];
		$data->location 	= $this->content['location'];

		$data->save();

		return $data;

	}

}

<?php 

namespace App\Jobs\Registration;

use Registration;
use App\Jobs\Job;

class CreateRegistration extends Job {

	public $content;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content) {

		$this->content = $content;
	}

	public function handle() {

		$input = [
			'firstname' 		=> $this->content['firstname'],
			'middle'   			=> $this->content['middle'],
			'lastname' 			=> $this->content['lastname'],
			'cellphone_number' 	=> $this->content['cellphone_number'],
			'landline_number' 	=> $this->content['landline_number'],
			'address' 			=> $this->content['address'],
			'status' 			=> $this->content['status'],
			'location' 			=> $this->content['location'],
		];

		return Registration::create($input);

	}

}

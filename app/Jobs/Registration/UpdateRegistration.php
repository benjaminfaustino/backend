<?php 

namespace App\Jobs\Registration;

use Registration;
use App\Jobs\Job;

class UpdateRegistration extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle() {
	
        $data = Registration::find($this->id);
       
        $data->firstname 	 	= $this->content['firstname'];
        $data->middle 	 		= $this->content['middle'];
        $data->lastname 	 	= $this->content['lastname'];
        $data->cellphone_number	= $this->content['cellphone_number'];
        $data->landline_number 	= $this->content['landline_number'];
        $data->address 	 		= $this->content['address'];
        $data->status 	 		= $this->content['status'];
        $data->location 	 	= $this->content['location'];

        $data->save();

        return $data;

	}

}

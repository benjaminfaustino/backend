<?php 

namespace App\Jobs\School;

use School;
use App\Jobs\Job;

use Uploader;

class CreateSchool extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/schools/');
		} else {
			$filename = "default.png";
		}

		$input = [
			'name' 				=> $this->content['name'],
			'description'   	=> $this->content['description'],
			'filename' 			=> $filename,
			'mission' 			=> $this->content['mission'],
			'vision' 			=> $this->content['vision'],
			'contact_number' 	=> $this->content['contact_number'],
			'address' 			=> $this->content['address'],
			'email' 			=> $this->content['email'],
		];

		return School::create($input);

	}

}

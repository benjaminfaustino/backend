<?php 

namespace App\Jobs\School;

use School;
use App\Jobs\Job;

use Uploader;

class UpdateSchool extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = School::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/schools/');
		} else {
			$filename = $data->filename;
		}

		$data->name 			= $this->content['name'];
		$data->description   	= $this->content['description'];
		$data->filename 		= $filename;
		$data->mission			= $this->content['mission'];
		$data->vision			= $this->content['vision'];
		$data->contact_number 	= $this->content['contact_number'];
		$data->address 			= $this->content['address'];
		$data->email 			= $this->content['email'];

        $data->save();

        return $data;

	}

}

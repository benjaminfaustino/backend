<?php 

namespace App\Jobs\Service;

use Service;
use App\Jobs\Job;

use Uploader;

class CreateService extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/services/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'title' 		=> $this->content['title'],
			'description'  	=> $this->content['description'],
			'filename' 		=> $filename,
			'sort' 			=> $this->content['sort'],
			'status' 		=> $this->content['status'],
			'location' 		=> $this->content['location'],
		];

		return Service::create($input);

	}

}

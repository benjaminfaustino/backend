<?php 

namespace App\Jobs\Staff;

use Staff;
use App\Jobs\Job;

use Uploader;

class CreateStaff extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/staffs/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'name' 			=> $this->content['name'],
			'description' 	=> $this->content['description'],
			'position' 		=> $this->content['position'],
			'filename' 		=> $filename,
			'sort' 			=> $this->content['sort'],
			'status' 		=> $this->content['status'],
			'location' 		=> $this->content['location'],
		];

		return Staff::create($input);

	}

}

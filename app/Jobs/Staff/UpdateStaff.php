<?php 

namespace App\Jobs\Staff;

use Staff;
use App\Jobs\Job;

use Uploader;

class UpdateStaff extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = Staff::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/staffs/');
		} else {
			$filename = $data->filename;
		}
       
		$data->name 		= $this->content['name'];
		$data->description 	= $this->content['description'];
		$data->position 	= $this->content['position'];
		$data->filename		= $filename;
		$data->sort 		= $this->content['sort'];
		$data->status 		= $this->content['status'];
		$data->location 	= $this->content['location'];

        $data->save();

        return $data;

	}

}

<?php 

namespace App\Jobs\Testimonial;

use Testimonial;
use App\Jobs\Job;

use Uploader;

class CreateTestimonial extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/testimonials/');
		} else {
			$filename = "placeholder.png";
		}

		$input = [
			'name' 		   => $this->content['name'],
			'description'  => $this->content['description'],
			'filename' 	   => $filename,
			'sort' 	       => $this->content['sort'],
			'status' 	   => $this->content['status'],
			'location' 	   => $this->content['location'],
		];

		return Testimonial::create($input);

	}

}

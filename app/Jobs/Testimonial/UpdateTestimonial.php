<?php 

namespace App\Jobs\Testimonial;

use Testimonial;
use App\Jobs\Job;

use Uploader;

class UpdateTestimonial extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
        $data = Testimonial::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/testimonials/');
		} else {
			$filename = $data->filename;
		}
       
		$data->name 	   = $this->content['name'];
		$data->description = $this->content['description'];
		$data->filename    = $filename;
		$data->sort 	   = $this->content['sort'];
		$data->status 	   = $this->content['status'];
		$data->location    = $this->content['location'];

        $data->save();

        return $data;

	}

}

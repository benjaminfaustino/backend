<?php 

namespace App\Jobs\User;

use User;
use App\Jobs\Job;

use Uploader;

class CreateUser extends Job {

	public $content, $image;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image) {

		$this->content = $content;
		$this->image   = $image;
	}

	public function handle(Uploader $Uploader) {

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/users/');
		} else {
			$filename = "placeholder.png";
		}

		if(isset($this->content['password'])) {
			$password = bcrypt( $this->content['password'] );
		} else {
			$password = '';
		}

		$input = [
			'firstname' 	=> $this->content['firstname'],
			'middle'  		=> $this->content['middle'],
			'lastname' 		=> $this->content['lastname'],
			'email' 		=> $this->content['email'],
			'password' 		=> $password,
			'location' 		=> $this->content['location'],
			'filename' 		=> $filename,
			'gender' 		=> $this->content['gender'],
			'role' 			=> $this->content['role'],
		];

		return User::create($input);

	}

}

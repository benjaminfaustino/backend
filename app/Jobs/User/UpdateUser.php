<?php 

namespace App\Jobs\User;

use User;
use App\Jobs\Job;

use Uploader;

class UpdateUser extends Job {

	public $content, $image, $id;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content, $image, $id) {

		$this->content = $content;
		$this->image   = $image;
		$this->id      = $id;

	}

	public function handle(Uploader $Uploader) {
	
		$data = User::find($this->id);

		if ($this->image) {
			$filename = $Uploader->upload($this->image, null, 'uploads/users/');
		} else {
			$filename = $data->filename;
		}

		if(isset($this->content['password'])) {
			$password = bcrypt( $this->content['password'] );
		} else {
			$password = '';
		}
       
		$data->firstname 	= $this->content['firstname'];
		$data->middle 		= $this->content['middle'];
		$data->lastname		= $this->content['lastname'];
		$data->email		= $this->content['email'];
		$data->password		= $password;
		$data->location		= $this->content['location'];
		$data->filename		= $filename;
		$data->gender		= $this->content['gender'];
		$data->role			= $this->content['role'];

        $data->save();

        return $data;

	}

}

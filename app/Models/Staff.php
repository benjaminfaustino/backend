<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use File;

class Staff extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = array('image');

     public $table = "staffs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'position', 'filename', 'sort', 'status', 'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getImageAttribute() {

        if(File::exists("uploads/staffs/" . $this->filename))
        {
            return "/uploads/staffs/" . $this->filename;
        }
        else
        {
            return "/images/placeholder.png";
        }
    }
}

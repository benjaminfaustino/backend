<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use File;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = array('image');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'middle', 'lastname', 'email', 'gender', 'password', 'location', 'filename', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getImageAttribute() {

        if(File::exists("uploads/users/" . $this->filename))
        {
            return "/uploads/users/" . $this->filename;
        }
        else
        {
            return "/images/placeholder.png";
        }
    }

    public function isSuperAdmin() {

        if ($this->role == "superadmin") return true;

        return false;
    }
}

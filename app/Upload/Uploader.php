<?php 

namespace App\Upload;

use Validator, Auth, Image, Uuid;

use App\Upload\UploaderInterface as UploaderInterface;

class Uploader implements UploaderInterface {

	public function upload($files, $id, $destination) {

		$response = "";
		
		foreach ($files as $file) {

			$type = explode("/", $file->getMimeType());
		
			if (!isset($type[0])) return false; 
			
			if ($type[0] == 'application') $type[0] = 'document';

			if ($type[0] == 'text') $type[0] = 'subtitle';
			
			if (!file_exists($destination)) mkdir($destination, 0777, true);

			$generateUUID = str_replace("-", "", Uuid::generate()->string);

			$generateFilename = $generateUUID . '.png';

			$getRealPath = Image::make($file->getRealPath());

			$getRealPath->save($destination . $generateFilename);

			$response = preg_replace('/\s+/', '', $generateFilename);
			
		}
		
		return $response;
		
	} 
}
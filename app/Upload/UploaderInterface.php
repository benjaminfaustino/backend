<?php

namespace App\Upload;

interface UploaderInterface {

    /**
     * Takes an array of files and attempts
     * to upload those files to the specified destination.
     *
	 * @param array $file
	 * @param string $filename
	 * @param string $destination
     * @return array $response
     */
	public function upload($file, $filename, $destination);

}
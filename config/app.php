<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Backbone'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://jpi.loc'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        Intervention\Image\ImageServiceProvider::class,

        Mavinoo\LaravelBatch\LaravelBatchServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        // Models
        'User'                              => App\Models\User::class,
        'Registration'                      => App\Models\Registration::class,
        'School'                            => App\Models\School::class,
        'Banner'                            => App\Models\Banner::class,
        'Service'                           => App\Models\Service::class,
        'Event'                             => App\Models\Event::class,
        'Testimonial'                       => App\Models\Testimonial::class,
        'Staff'                             => App\Models\Staff::class,
        'Gallery'                           => App\Models\Gallery::class,

        // Request
        'CreateRegistrationRequest'         => App\Http\Requests\Registrations\CreateRegistrationRequest::class,
        'UpdateRegistrationRequest'         => App\Http\Requests\Registrations\UpdateRegistrationRequest::class,

        'CreateSchoolRequest'               => App\Http\Requests\Schools\CreateSchoolRequest::class,
        'UpdateSchoolRequest'               => App\Http\Requests\Schools\UpdateSchoolRequest::class,

        'CreateBannerRequest'               => App\Http\Requests\Banners\CreateBannerRequest::class,
        'UpdateBannerRequest'               => App\Http\Requests\Banners\UpdateBannerRequest::class,

        'CreateServiceRequest'              => App\Http\Requests\Services\CreateServiceRequest::class,
        'UpdateServiceRequest'              => App\Http\Requests\Services\UpdateServiceRequest::class,

        'CreateEventRequest'                => App\Http\Requests\Events\CreateEventRequest::class,
        'UpdateEventRequest'                => App\Http\Requests\Events\UpdateEventRequest::class,

        'CreateTestimonialRequest'          => App\Http\Requests\Testimonials\CreateTestimonialRequest::class,
        'UpdateTestimonialRequest'          => App\Http\Requests\Testimonials\UpdateTestimonialRequest::class,

        'CreateStaffRequest'                => App\Http\Requests\Staffs\CreateStaffRequest::class,
        'UpdateStaffRequest'                => App\Http\Requests\Staffs\UpdateStaffRequest::class,

        'CreateUserRequest'                 => App\Http\Requests\Users\CreateUserRequest::class,
        'UpdateUserRequest'                 => App\Http\Requests\Users\UpdateUserRequest::class,

        'CreateGalleryRequest'              => App\Http\Requests\Galleries\CreateGalleryRequest::class,
        'UpdateGalleryRequest'              => App\Http\Requests\Galleries\UpdateGalleryRequest::class,

        // Jobs
        'CreateUser'                        => App\Jobs\User\CreateUser::class,
        'UpdateUser'                        => App\Jobs\User\UpdateUser::class,

        'CreateRegistration'                => App\Jobs\Registration\CreateRegistration::class,
        'UpdateRegistration'                => App\Jobs\Registration\UpdateRegistration::class,

        'CreateSchool'                      => App\Jobs\School\CreateSchool::class,
        'UpdateSchool'                      => App\Jobs\School\UpdateSchool::class,

        'CreateBanner'                      => App\Jobs\Banner\CreateBanner::class,
        'UpdateBanner'                      => App\Jobs\Banner\UpdateBanner::class,

        'CreateService'                     => App\Jobs\Service\CreateService::class,
        'UpdateService'                     => App\Jobs\Service\UpdateService::class,

        'CreateEvent'                       => App\Jobs\Event\CreateEvent::class,
        'UpdateEvent'                       => App\Jobs\Event\UpdateEvent::class,

        'CreateTestimonial'                 => App\Jobs\Testimonial\CreateTestimonial::class,
        'UpdateTestimonial'                 => App\Jobs\Testimonial\UpdateTestimonial::class,

        'CreateStaff'                       => App\Jobs\Staff\CreateStaff::class,
        'UpdateStaff'                       => App\Jobs\Staff\UpdateStaff::class,

        'CreateGallery'                     => App\Jobs\Gallery\CreateGallery::class,
        'UpdateGallery'                     => App\Jobs\Gallery\UpdateGallery::class,

        // Image Intervention
        'Image'                             => Intervention\Image\Facades\Image::class,

        // Laravel UUID
        'Uuid'                              => Webpatser\Uuid\Uuid::class,

        // Uploader
        'Uploader'                          => App\Upload\Uploader::class,
        'MultipleUploader'                  => App\Upload\MultipleUploader::class,

        // Others
        'Input'                             => Illuminate\Support\Facades\Input::class,

        'Batch'                             => Mavinoo\LaravelBatch\LaravelBatchFacade::class,

        'Controller'                        => App\Http\Controllers\Controller::class,

    ],

];

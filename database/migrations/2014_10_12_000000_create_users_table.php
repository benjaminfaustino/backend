<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('firstname');
            $table->text('middle');
            $table->text('lastname');
            $table->string('gender');
            $table->string('email')->unique();
            $table->string('password');
            $table->text('location');
            $table->text('filename');
            $table->text('role');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('users')->insert(array(
            "firstname" => "Admin", 
            "middle" => "A",
            "lastname" => "Admin",
            "email" => "superadmin@admin.com",
            "gender" => "Male",
            "password" => Hash::make("superadminfreelance"),
            "filename" => "default.png",
            "location" => "superadmin",
            "role" => "superadmin",
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s")
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

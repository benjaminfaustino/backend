 new Vue({
  el: '#header',
  data: {
      url: 'admin-panel/registrations',
      countNewRegistrator: 0,
  },
  created() {

    this.fetch();

  },

  mounted() {

  },

  computed: {
    
  },

  methods: {

    fetch() {

      axios.post('/' + this.url + '/count')
        .then(response => {

          this.countNewRegistrator = parseInt(response.data.data);

          $(".countNewRegistrator").removeClass("hidden");

          $(".countNewRegistrator").html(this.countNewRegistrator);

          this.fetchWithTimeout();
          
        })
        .catch(error => {
          
        });

    },

    fetchWithTimeout() {

    	var self = this;

    	setTimeout(function(){

	      axios.post('/' + self.url + '/count')
	        .then(response => {

	          self.countNewRegistrator = parseInt(response.data.data);

	          $(".countNewRegistrator").html(self.countNewRegistrator);
	          
	        })
	        .catch(error => {
	          
	        });

    	}, 60000);

    },

  }
});
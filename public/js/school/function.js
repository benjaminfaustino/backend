(function($) {


    function Backbone() {}


    Backbone.prototype = {
        constract: Backbone,

        mobileMenuClick: function() {

        },

    }

   
    window.Backbone = Backbone;


}(jQuery));


var backbone = new Backbone();

$(window).on("load", function() {
    $(document).on('click', '.mobile-menu', backbone.mobileMenuClick);

    $(".nav-list").removeClass("active");
    // Change left sidebar active selection
    $("#school").addClass("active");
});

$(window).resize(function() {

});

// Vue

// Select 2 component initialization
Vue.component('select2', {
    props: ['options', 'value'],
    template: '<select><slot></slot></select>',
    mounted: function () {
        var vm = this
        $(this.$el)
            // init select2
            .select2({
                data: this.options,
                placeholder: "Search",
                allowClear: true
            })
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {

              if (this.value) {

                $(".collection-listing").addClass("hidden");

                $("#" + this.value + ".collection-listing").removeClass("hidden");

              } else {

                $(".collection-listing").removeClass("hidden");                

              }

            });

    },
    methods: {
    },
    watch: {
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

new Vue({
  el: '#app',
  data: {
      url: 'admin-panel/schools',
      collections: [],
      collect: {
        id: "",
        name: "",
        description: "",
        mission: "",
        vision: "",
        contact_number: "",
        address: "",
        email: "",
        index: "",
        filename: "",
      },
      buttonActive: true,
      errors: [],
      formData: [],
      reference: true,
  },
  created() {

    this.fetch();

  },

  mounted() {

  },

  computed: {
    
  },

  methods: {

    fetch() {


      axios.post('/' + this.url + '/get')
        .then(response => {

          this.collections = response.data.data;

          $("section.content").removeClass("hidden");
          $(".loader").addClass("hidden");
          
        })
        .catch(error => {
          
        });

    },

    edit(item, index) {

      this.collect.id             = item.id;
      this.collect.name           = item.name;
      this.collect.description    = item.description;
      this.collect.mission        = item.mission;
      this.collect.vision         = item.vision;
      this.collect.contact_number = item.contact_number;
      this.collect.address        = item.address;
      this.collect.email          = item.email;
      this.collect.index          = index;
      this.collect.filename       = item.filename;

      this.buttonActive = false;

      $("#image-profile").val("");

    },

    add() {

      this.reset();

      this.buttonActive = true;

    },

    save() {

      axios.post('/' + this.url, this.setFormData() )
        .then(response => {

          this.collections.unshift(response.data.data);

          this.reset();

          $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {

          if (error.response.data.errors) this.errors = error.response.data.errors;
          
        });

    },

    update() {

      axios.post('/' + this.url + '/put/' + this.collect.id, this.setFormData() )
        .then(response => {

          this.updateData(response);

          $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {
          
      });

    },

    remove(item, index) {

      if(confirm("Are you sure?")) {

        axios.delete('/' + this.url + '/' + item.id)
          .then(response => {

            this.collections.splice(index, 1);

            this.reference = this.generateDummyId();
            
          })
          .catch(error => {
            
        });

      }

    },

    updateData(response) {

      this.collections[this.collect.index].id             = response.data.data.id;
      this.collections[this.collect.index].name           = response.data.data.name;
      this.collections[this.collect.index].description    = response.data.data.description;
      this.collections[this.collect.index].mission        = response.data.data.mission;
      this.collections[this.collect.index].vision         = response.data.data.vision;
      this.collections[this.collect.index].contact_number = response.data.data.contact_number;
      this.collections[this.collect.index].address        = response.data.data.address;
      this.collections[this.collect.index].email          = response.data.data.email;
      this.collections[this.collect.index].filename       = response.data.data.filename;
      this.collections[this.collect.index].image          = response.data.data.image;
      this.collections[this.collect.index].index          = this.collect.index;

    },

    reset() {

      this.collect = {
        id: "",
        name: "",
        description: "",
        mission: "",
        vision: "",
        contact_number: "",
        address: "",
        email: "",
        index: "",
        filename: "",
      }

    },

    setFormData() {

      var fileInput = document.querySelector('#image-profile');

      this.formData = [];

      this.formData = new FormData();
      this.formData.append('id', this.collect.id);
      this.formData.append('name', this.collect.name);
      this.formData.append('description', this.collect.description);
      this.formData.append('mission', this.collect.mission);
      this.formData.append('vision', this.collect.vision);
      this.formData.append('contact_number', this.collect.contact_number);
      this.formData.append('address', this.collect.address);
      this.formData.append('email', this.collect.email);
      this.formData.append('image', fileInput.files[0]);
      this.formData.append('index', this.collect.index);
      this.formData.append('filename', this.collect.filename);

      return this.formData;

    },

    generateDummyId() {

      return Math.floor((Math.random() * 1000) + 1);

    },

  }
});
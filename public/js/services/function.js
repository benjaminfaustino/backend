(function($) {


    function Backbone() {}


    Backbone.prototype = {
        constract: Backbone,

        mobileMenuClick: function() {

        },

    }

   
    window.Backbone = Backbone;


}(jQuery));


var backbone = new Backbone();

$(window).on("load", function() {
    $(document).on('click', '.mobile-menu', backbone.mobileMenuClick);

    $(".nav-list").removeClass("active");
    // Change left sidebar active selection
    $("#services").addClass("active");
});

$(window).resize(function() {

});

// Vue

// Select 2 component initialization
Vue.component('select2', {
    props: ['options', 'value'],
    template: '<select><slot></slot></select>',
    mounted: function () {
        var vm = this
        $(this.$el)
            // init select2
            .select2({
                placeholder: "Search",
                allowClear: true
            })
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {

              if (this.value) {

                $(".collection-listing").addClass("hidden");

                $("#" + this.value + ".collection-listing").removeClass("hidden");

              } else {

                $(".collection-listing").removeClass("hidden");                

              }

            });

    },
    methods: {
    },
    watch: {
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

Vue.component('selectstatus', {
    props: ['options', 'value'],
    template: '<select id="select2-app"><slot></slot></select>',
    mounted: function () {
        var vm = this
        $(this.$el)
            // init select2
            .select2({
            })
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {

                vm.$parent.collect.status = this.value;

            });

    },
    methods: {
    },
    watch: {
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

// Select 2 component initialization
Vue.component('selectlocation', {
    props: ['options', 'value'],
    template: '<select id="select2-location"><slot></slot></select>',
    mounted: function () {
        var vm = this
        $(this.$el)
            // init select2
            .select2({
                placeholder: "Select Location",
                allowClear: true
            })
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {

              vm.$parent.collect.location = this.value;

            });

    },
    methods: {
    },
    watch: {
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});


new Vue({
  el: '#app',
  data: {
      url: 'admin-panel/services',
      collections: [],
      collect: {
        id: "",
        title: "",
        description: "",
        filename: "",
        sort: "",
        status: "",
        location: "",
        image: "",
        index: "",
      },
      buttonActive: true,
      errors: [],
      formData: [],
      reference: true,
      locations: [],
  },
  created() {

    this.fetch();

    this.locations = JSON.parse(locations);

  },

  mounted() {

    this.sortablefunction();

  },

  computed: {
    
  },

  methods: {

    fetch() {


      axios.post('/' + this.url + '/get')
        .then(response => {

          this.collections = response.data.data;

          $("section.content").removeClass("hidden");
          $(".loader").addClass("hidden");
          
        })
        .catch(error => {
          
        });

    },

    edit(item, index) {

      this.collect.id          = item.id;
      this.collect.title       = item.title;
      this.collect.description = item.description;
      this.collect.filename    = item.filename;
      this.collect.sort        = item.sort;
      this.collect.status      = item.status;
      this.collect.location    = item.location;
      this.collect.image       = item.image;
      this.collect.index       = index;

      this.buttonActive = false;

      $("#image-profile").val("");

      $('#select2-app').val(item.status).trigger('change');

      $('#select2-location').val(item.location).trigger('change');

    },

    add() {

      this.reset();

      this.buttonActive = true;

      $('#select2-app').val('on').trigger('change');

      $('#select2-location').val('').trigger('change');

    },

    save() {

      axios.post('/' + this.url, this.setFormData(true) )
        .then(response => {

          this.collections.push(response.data.data);

          this.reset();

          $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {

          if (error.response.data.errors) this.errors = error.response.data.errors;
          
        });

    },

    update() {

      axios.post('/' + this.url + '/put/' + this.collect.id, this.setFormData(false) )
        .then(response => {

          this.updateData(response);

          $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {
          
      });

    },

    remove(item, index) {

      if(confirm("Are you sure?")) {

        axios.delete('/' + this.url + '/' + item.id)
          .then(response => {

            this.collections.splice(index, 1);

            this.reference = this.generateDummyId();
            
          })
          .catch(error => {
            
        });

      }

    },

    updateData(response) {

      this.collections[this.collect.index].id             = response.data.data.id;
      this.collections[this.collect.index].title          = response.data.data.title;
      this.collections[this.collect.index].description    = response.data.data.description;
      this.collections[this.collect.index].filename       = response.data.data.filename;
      this.collections[this.collect.index].sort           = response.data.data.sort;
      this.collections[this.collect.index].status         = response.data.data.status;
      this.collections[this.collect.index].location       = response.data.data.location;
      this.collections[this.collect.index].image          = response.data.data.image;
      this.collections[this.collect.index].index          = this.collect.index;

    },

    reset() {

      this.collect = {
        id: "",
        title: "",
        description: "",
        filename: "",
        sort: "",
        status: "",
        location: "",
        image: "",
        index: "",
      }

    },

    setFormData(boolean) {

      var fileInput = document.querySelector('#image-profile');

      this.formData = [];

      this.formData = new FormData();
      this.formData.append('id', this.collect.id);
      this.formData.append('title', this.collect.title);
      this.formData.append('description', this.collect.description);
      this.formData.append('filename', this.collect.filename);
      this.formData.append('sort', (boolean) ? this.collections.length + 1 : this.collect.sort);
      this.formData.append('status', this.collect.status);
      this.formData.append('location', this.collect.location);
      this.formData.append('image', fileInput.files[0]);
      this.formData.append('index', this.collect.index);

      return this.formData;

    },

    generateDummyId() {    

      return Math.floor((Math.random() * 1000) + 1);

    },

    orderBy: function (arr) {
        // Set slice() to avoid to generate an infinite loop!
        return arr.slice().sort(function (a, b) {
              return a.sort - b.sort;
        });
    },

    sortablefunction() {

      var self = this;

      $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
        axis: 'y',
        update: function( ) {

            var data = [];

            $($(this).find('.collection-listing')).each(function(index) {
                
                $ID = $(this).attr("id");

                self.collections.filter(function(collect){

                  if($ID == collect.id) {

                    collect.sort = index;

                    data.push({
                      'id': collect.id,
                      'sort': collect.sort
                    });

                  }

                });

            });

            axios.post('/' + self.url + '/bulk', data)
              .then(response => {
                
              })
              .catch(error => {
                
            });

        }
      });

    }

  }
});
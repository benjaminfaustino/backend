<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>JPI @yield("title")</title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/ionicons.min.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/jquery-jvectormap.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/AdminLTE.min.css') }}">

    <!-- <link rel="stylesheet" href="{{ asset('/css/plugins/jquery-ui.css') }}"> -->

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('/css/plugins/all-skins.min.css') }}">

    <link rel="stylesheet" type="text/css" href="/css/plugins/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="{{ asset('/css/plugins/select2.min.css') }}">
    
    @yield("stylesheets")

    <!-- Overide CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/global.css') }}">

    <!-- <link rel="icon" href="{{ asset('/images/headers/favicon.ico') }}"> -->

</head>
<body id="body" class="skin-black sidebar-mini">

    <div class="loader">Loading...</div>

    <!-- Content -->
    <div id="app" class="wrapper">

        @if(Auth::check())

            @include('admin/layouts/header')

            @include('admin/layouts/left-sidebar')

            @yield("content")

            <div class="error-message" v-if="errors">
                    
                    @{{ errors }}

            </div>

        @endif

    </div>

    <script type="text/javascript">
      
        var locations = {!! json_encode(File::get(public_path() . "/locations/locations.json" )) !!};

    </script>

    <!-- jQuery 3 -->
    <script type="text/javascript" src="{{ asset('/js/plugins/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script type="text/javascript" src="{{ asset('/js/plugins/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/vue.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/axios.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/global.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/jquery-ui.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/moment.js') }}"></script>

    <!-- FastClick -->
    <script type="text/javascript" src="{{ asset('/js/plugins/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script type="text/javascript" src="{{ asset('/js/plugins/adminlte.min.js') }}"></script>
    <!-- Sparkline -->
    <script type="text/javascript" src="{{ asset('/js/plugins/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap  -->
    <script type="text/javascript" src="{{ asset('/js/plugins/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- SlimScroll -->
    <script type="text/javascript" src="{{ asset('/js/plugins/jquery.slimscroll.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script type="text/javascript" src="{{ asset('/js/plugins/dashboard2.js') }}"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script type="text/javascript" src="{{ asset('/js/plugins/demo.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        axios.create({
          baseURL: 'https://some-domain.com/api/',
          timeout: 1000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    </script>


    @yield("scripts")

    <!-- Overide JS -->
    <!-- <script type="text/javascript" src="{{ asset('/js/global.js') }}"></script> -->
    
</body>
</html>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('/images/default.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
<!--         <li id="dashboard" class="nav-list">
          <a href="{{ url('admin-panel') }}">
            <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> -->

        <li id="banner" class="nav-list">
          <a href="{{ url('admin-panel/banners') }}">
            <i class="fas fa-file-image"></i> <span>Banner</span>
          </a>
        </li>

        <li id="events" class="nav-list">
          <a href="{{ url('admin-panel/events') }}">
            <i class="fas fa-calendar-alt"></i> <span>Events</span>
          </a>
        </li>

        <li id="galleries" class="nav-list">
          <a href="{{ url('admin-panel/galleries') }}">
            <i class="fas fa-images"></i> <span>Gallery</span>
          </a>
        </li>

        <li id="registrations" class="nav-list">
          <a href="{{ url('admin-panel/registrations') }}">
            <i class="fas fa-registered"></i> <span>Registration</span>
          </a>
        </li>

        <li id="school" class="nav-list">
          <a href="{{ url('admin-panel/schools') }}">
            <i class="fas fa-school"></i> <span>School</span>
          </a>
        </li>


        <li id="services" class="nav-list">
          <a href="{{ url('admin-panel/services') }}">
            <i class="fas fa-list-alt"></i> <span>Service</span>
          </a>
        </li>

        <li id="staff" class="nav-list">
          <a href="{{ url('admin-panel/staffs') }}">
            <i class="fas fa-users"></i> <span>Staff</span>
          </a>
        </li>

        <li id="testimonials" class="nav-list">
          <a href="{{ url('admin-panel/testimonials') }}">
            <i class="fas fa-bullhorn"></i> <span>Testimonial</span>
          </a>
        </li>

        @if(Auth::check() && Auth::user()->isSuperAdmin())

        <li id="user" class="nav-list">
          <a href="{{ url('admin-panel/users') }}">
            <i class="fas fa-user"></i> <span>User</span>
          </a>
        </li>

        @endif

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
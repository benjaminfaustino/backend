@extends('admin.layouts.backend')

@section('title', 'Staffs')

@section('stylesheets')

@stop

@section('content')

  <div class="content-wrapper">

	<!-- Main content -->
    <section class="content hidden">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                  <select2 class="search form-control" :key="reference">
                      <option></option>
                      <option v-for="(collection, index) in collections" :value="collection.id">@{{ collection.name }}</option>
                  </select2>
              </h3>

              <div class="pull-right">
                  <button 
                    type="button" 
                    class="btn btn-info btn-flat"  
                    data-toggle="modal" 
                    data-target="#modal-app"
                    @click="add()">
                      <i class="fas fa-plus"></i>
                  </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Position</th>
                  <th>Status</th>
                  <th>Location</th>
                </tr>
                </thead>
                <tbody id="sortable">

                	<tr v-for="(item, index) in orderBy(collections)" class="collection-listing  ui-state-default" :id="item.id">
                    <td class="td-width image"> 
                      <img :src="item.image">
                    </td> 
                    <td class="td-width"> @{{ item.name }} </td> 
                    <td class="td-width"> @{{ item.description }} </td> 
                    <td class="td-width"> @{{ item.position }} </td> 
                    <td class="td-width"> @{{ item.status }} </td>
                    <td class="td-width"> @{{ item.location }} </td> 
                    <td class="td-width center">
                      <div class="btn-group">

                        <button 
                          type="button" 
                          class="btn btn-info btn-flat"  
                          data-toggle="modal" 
                          data-target="#modal-app"
                          @click="edit(item, index)">
                            <i class="fas fa-edit"></i>
                        </button>

                        <button 
                          type="button" 
                          class="btn btn-danger btn-flat"
                          @click="remove(item, index)">
                            <i class="fas fa-trash-alt"></i>
                        </button>

                      </div>
                    </td>
                  </tr>

            	</tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  		  </div>
  	  </div>
  	</section>

    <div id="modal-app" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <!-- <h5 class="modal-title">Edit</h5> -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="">
              <div class="box-body">

                <div class="form-group">
                  <label>Choose Image:</label>
                  <input id="image-profile" type="file" name="" class="form-control">
                </div>

                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" v-model="collect.name">
                </div>

                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="4" cols="50" v-model="collect.description"> @{{ collect.description }} </textarea>
                </div>

                <div class="form-group">
                  <label>Position</label>
                  <input type="text" class="form-control" v-model="collect.position">
                </div>

                <div class="form-group">
                  <label>Status</label>

                  <div>
                    <selectstatus class="search form-control" v-model="collect.status">
                        <option value="on">On</option>
                        <option value="off">Off</option>
                    </selectstatus>
                  </div>
                </div>

                <div class="form-group">
                  <label>Location</label>

                  <div>
                    <selectlocation class="search form-control" v-model="collect.status">

                        <option></option>
                        <option v-for="(location, index) in locations" :value="location"> @{{ location }} </option>

                    </selectlocation>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Cancel</button>

            <template v-if="buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="save()">Save</button>
            </template>
            <template v-if="!buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="update()">Update</button>
            </template>

          </div>
        </div>
      </div>
    </div>

  </div>

@endsection

@section('scripts')
	
	<script type="text/javascript" src="{{ asset('/js/staff/function.js') }}"></script>

@stop
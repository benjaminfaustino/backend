@extends('admin.layouts.backend')

@section('title', 'Users')

@section('stylesheets')

@stop

@section('content')

  <div class="content-wrapper">

	<!-- Main content -->
    <section class="content hidden">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                  <select2 class="search form-control" :key="reference">
                      <option></option>
                      <option v-for="(collection, index) in collections" :value="collection.id">@{{ collection.name }}</option>
                  </select2>
              </h3>

              <div class="pull-right">
                  <button 
                    type="button" 
                    class="btn btn-info btn-flat"  
                    data-toggle="modal" 
                    data-target="#modal-app"
                    @click="add()">
                      <i class="fas fa-plus"></i>
                  </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Email</th>
                  <th>Location</th>
                </tr>
                </thead>
                <tbody id="sortable">

                	<tr v-for="(item, index) in orderBy(collections)" class="collection-listing  ui-state-default" :id="item.id">
                    <td class="td-width image"> 
                      <img :src="item.image">
                    </td> 
                    <td class="td-width"> @{{ item.firstname }} @{{ item.middle }} @{{ item.lastname }}</td> 
                    <td class="td-width"> @{{ item.gender }} </td> 
                    <td class="td-width"> @{{ item.email }} </td> 
                    <td class="td-width"> @{{ item.location }} </td> 
                    <td class="td-width center">
                      <div class="btn-group">

                        <button 
                          type="button" 
                          class="btn btn-info btn-flat"  
                          data-toggle="modal" 
                          data-target="#modal-app"
                          @click="edit(item, index)">
                            <i class="fas fa-edit"></i>
                        </button>

                        <button 
                          type="button" 
                          class="btn btn-danger btn-flat"
                          @click="remove(item, index)">
                            <i class="fas fa-trash-alt"></i>
                        </button>

                      </div>
                    </td>
                  </tr>

            	</tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  		  </div>
  	  </div>
  	</section>

    <div id="modal-app" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <!-- <h5 class="modal-title">Edit</h5> -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="">
              <div class="box-body">

                <div class="form-group">
                  <label>Choose Image:</label>
                  <input id="image-profile" type="file" name="" class="form-control">
                </div>

                <div class="form-group">
                  <label>Firstname</label>
                  <input type="text" class="form-control" v-model="collect.firstname">
                </div>

                <div class="form-group">
                  <label>Middle</label>
                  <input type="text" class="form-control" v-model="collect.middle">
                </div>

                <div class="form-group">
                  <label>Lastname</label>
                  <input type="text" class="form-control" v-model="collect.lastname">
                </div>

                <div class="form-group">
                  <label>Gender</label>

                  <div>
                    <selectgender class="search form-control" v-model="collect.gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </selectgender>
                  </div>
                </div>

                <div class="form-group">
                  <label>Email</label>
                  <input type="text" class="form-control" v-model="collect.email">
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" v-model="collect.password">
                </div>

                <div class="form-group">
                  <label>Role</label>

                  <div>
                    <selectrole class="search form-control" v-model="collect.role">
                        <option value="male">Admin</option>
                        <option value="female">Student</option>
                    </selectrole>
                  </div>
                </div>


                <div class="form-group">
                  <label>Location</label>

                  <div>
                    <selectlocation class="search form-control" v-model="collect.status">

                        <option></option>
                        <option v-for="(location, index) in locations" :value="location"> @{{ location }} </option>

                    </selectlocation>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Cancel</button>

            <template v-if="buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="save()">Save</button>
            </template>
            <template v-if="!buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="update()">Update</button>
            </template>

          </div>
        </div>
      </div>
    </div>

  </div>

@endsection

@section('scripts')
	
	<script type="text/javascript" src="{{ asset('/js/user/function.js') }}"></script>

@stop
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return redirect('/');
});

Route::get('/admin-panel/', function () {
    return redirect('/admin-panel/login');
});

Route::get('/home', function () {
    return redirect('/');
});

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', 'Index\IndexController@index');

Route::get('admin-panel/login', 'Auth\LoginController@showLoginForm');
Route::post('admin-panel/login', 'Auth\LoginController@login');
Route::get('admin-panel/register', 'Auth\RegisterController@showRegistrationForm');

Route::resource('registrations', 'Admin\RegistrationController');

Route::group([
	'middleware' => 'auth', 
	'prefix' => 'admin-panel'], 
function () {

	Route::get('/', function () {
	    return redirect('/admin-panel/registrations');
	});

	# Banner
	Route::post('banners/put/{id}', 'Admin\BannerController@update');
	Route::post('banners/get', 'Admin\BannerController@get');
	Route::post('banners/bulk', 'Admin\BannerController@bulk');

	Route::resource('banners', 'Admin\BannerController');

	# Events
	Route::post('events/put/{id}', 'Admin\EventController@update');
	Route::post('events/get', 'Admin\EventController@get');
	Route::post('events/bulk', 'Admin\EventController@bulk');

	Route::resource('events', 'Admin\EventController');

	# Galleries
	Route::post('galleries/put/{id}', 'Admin\GalleryController@update');
	Route::post('galleries/get', 'Admin\GalleryController@get');
	Route::post('galleries/bulk', 'Admin\GalleryController@bulk');

	Route::resource('galleries', 'Admin\GalleryController');

	# Register
	Route::post('registrations/put/{id}', 'Admin\RegistrationController@update');
	Route::post('registrations/get', 'Admin\RegistrationController@get');
	Route::post('registrations/bulk', 'Admin\RegistrationController@bulk');
	Route::post('registrations/count', 'Admin\RegistrationController@count');

	Route::resource('registrations', 'Admin\RegistrationController');

	# Testimonials
	Route::post('testimonials/put/{id}', 'Admin\TestimonialController@update');
	Route::post('testimonials/get', 'Admin\TestimonialController@get');
	Route::post('testimonials/bulk', 'Admin\TestimonialController@bulk');

	Route::resource('testimonials', 'Admin\TestimonialController');

	# Services
	Route::post('services/put/{id}', 'Admin\ServiceController@update');
	Route::post('services/get', 'Admin\ServiceController@get');
	Route::post('services/bulk', 'Admin\ServiceController@bulk');

	Route::resource('services', 'Admin\ServiceController');

	# School
	Route::post('schools/put/{id}', 'Admin\SchoolController@update');
	Route::post('schools/get', 'Admin\SchoolController@get');

	Route::resource('schools', 'Admin\SchoolController');

	# Staffs
	Route::post('staffs/put/{id}', 'Admin\StaffController@update');
	Route::post('staffs/get', 'Admin\StaffController@get');
	Route::post('staffs/bulk', 'Admin\StaffController@bulk');

	Route::resource('staffs', 'Admin\StaffController');

	# User
	Route::post('users/put/{id}', 'Admin\UserController@update');
	Route::post('users/get', 'Admin\UserController@get');
	Route::post('users/bulk', 'Admin\UserController@bulk');

	Route::resource('users', 'Admin\UserController');

});

# USAGE
# Actions Handled By Resource Controller

# Verb		URI						Action		Route Name
#GET		/photos					index		photos.index
#GET		/photos/create			create		photos.create
#POST		/photos					store		photos.store
#GET		/photos/{photo}			show		photos.show
#GET		/photos/{photo}/edit	edit		photos.edit
#PUT/PATCH	/photos/{photo}			update		photos.update
#DELETE		/photos/{photo}			destroy		photos.destroy

// Auth::routes();